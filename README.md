# What?

This repository will contain the main text of my Master's thesis "Hybrid Raytracing in DXR", while working on my degree on the Brno University of Technology, Faculty of information Technology. The main goal is to answer the question "Is it worth it implementing the DXR API for new games?"
